require 'singleton'

module Clicksignwebdoxapi
  class Configuration
    include Singleton

    attr_accessor :token

    def initialize
      @token = nil
      @token_set = false
    end

    def token=(new_token)
      raise 'Token already set' if @token_set

      @token = new_token
      @token_set = true
    end

    def self.configure
      yield instance
    end
  end

  def self.configure
    Configuration.configure { |config| yield config }
  end

  class << self
    attr_accessor :configuration
  end
end
