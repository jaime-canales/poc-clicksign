require 'rest-client'
require_relative 'configuration'

module Clicksignwebdoxapi
  class Client
    BASE_URL = 'https://sandbox.clicksign.com/api/v3'
    #attr_reader :configuration, :headers

    # initialize object
    # client = Clicksignwebdoxapi::Client.new
    # client.create_envelope(name: "prueba envelope")
    def initialize
      @configuration = Configuration.instance
      raise "not token provided" unless @configuration.token

      @headers = {
        Authorization: @configuration.token,
        'Content-Type': 'application/vnd.api+json'
      }
    end

    def create_envelope(name:, locale: 'pt-BR', auto_close: true, remind_interval: nil, block_after_refusal: true, deadline_at: DateTime.now.tomorrow.to_s)
      endpoint = "#{BASE_URL}/envelopes"
      payload = {
        data: {
          type: 'envelopes',
          attributes: {
            name: name,
            locale: locale,
            auto_close: auto_close,
            remind_interval: remind_interval,
            block_after_refusal: block_after_refusal,
            deadline_at: deadline_at
          }
        }
      }
      response = RestClient.post(endpoint, payload.to_json, @headers)
      JSON.parse(response.body)
      rescue RestClient::ExceptionWithResponse => e
        e.response
      end
    end


=begin
#client = Clicksignwebdoxapi::Client.create_envelope(name: "prueba envelope")
class_methods
    class << self
    private def headers
      {
        Authorization: Configuration.instance.token,
        'Content-Type': 'application/vnd.api+json'
      }
    end

    def create_envelope(name:, locale: 'pt-BR', auto_close: true, remind_interval: nil, block_after_refusal: true, deadline_at: DateTime.now.tomorrow.to_s)
      endpoint = "#{BASE_URL}/envelopes"
      payload = {
        data: {
          type: 'envelopes',
          attributes: {
            name: name,
            locale: locale,
            auto_close: auto_close,
            remind_interval: remind_interval,
            block_after_refusal: block_after_refusal,
            deadline_at: deadline_at
          }
        }
      }
      response = RestClient.post(endpoint, payload.to_json, headers)
      JSON.parse(response.body)
      rescue RestClient::ExceptionWithResponse => e
        e.response
      end
    end
  end
=end
end
