require 'rails/generators/base'

# run rails g clicksignwebdoxapi:install
module Clicksignwebdoxapi
  module Generators
    class InstallGenerator < Rails::Generators::Base
      source_root File.expand_path("../../templates", __FILE__)

      desc "Creates a Clicksignwebdoxapi initializer."
      def copy_initializer
        template "clicksignwebdoxapi.rb", "config/initializers/clicksignwebdoxapi.rb"
      end
    end
  end
end
