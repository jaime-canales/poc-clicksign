# frozen_string_literal: true

require_relative "clicksignwebdoxapi/version"
require_relative "clicksignwebdoxapi/client"
require_relative "clicksignwebdoxapi/configuration"

module Clicksignwebdoxapi
  class Error < StandardError; end
end
